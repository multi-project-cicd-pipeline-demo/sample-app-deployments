# Read the string argument and store it in a variable
img_path="$1"

# Alternative 
tag=$(wget -qO- $img_path | grep -o '"name":"[^"]*' | awk -F ':"' 'NR==1{print $2}')
# tag=$(curl -s $img_path | grep -o '"name":"[^"]*' | awk -F ':"' '{print $2}')

echo "$tag"
